FROM strapi/base
WORKDIR /app

COPY ./rems_server/package.json .
RUN npm install

COPY ./rems_server .
EXPOSE 1337

CMD ["npm", "start"]